# PS-mpay Deployment
# 1. Docker installation

Steps to install docker in ubuntu:
----------------------------------
- To install docker copy and paste below command in terminal
```
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
```
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```
```
echo "deb [signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
```
sudo apt update
```
```
sudo apt install -y docker-ce docker-ce-cli containerd.io
```
```
sudo usermod -aG docker $USER
```
- To check which version of docker installed
```
sudo docker --version
```
- To check docker installed sucessfully
```
sudo docker run hello-world
```
```
reboot
```

# 2. Postgres Installation

Steps to install postgresql-client:
-----------------------------------
```
sudo apt-get update
```
```
sudo apt-get install postgresql-client-common postgresql-client-12
```
if issues raised while trying to install postgresql-client-common postgresql-client-12
-----------------------------------------------------------------------------------
<img src="Screenshots/postgres-installton-error.png">

Then installed postgresql-client-common postgresql-client-14:
-----------------------------------------------------------
```
sudo apt-get install postgresql-client-common postgresql-client-14
```
To run postgres container:
------------------------- 
```
docker run --name postgres -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=P@ssw0rd -d postgres:12
```
To connect postgres db:
-----------------------
```
psql -U postgres -h localhost -p 5432 -d postgres

Note: password: P@ssw0rd
```
To create user(k3s), schema,role and search_path in postgres
-------------------------------------------------------
```
create user k3s with password 'k3s';
```
```
create schema k3s authorization k3s;
```
```
alter user k3s with superuser;
```
```
alter user k3s set search_path to 'k3s';

```
To create user(mepsyemen),role and search_path in postgres
------------------------------------------------------------
```
CREATE USER mepsyemen WITH PASSWORD 'mepsyemen';
```
```
ALTER USER mepsyemen WITH SUPERUSER;
```
```
ALTER USER mepsyemen SET search_path to 'mepsyemen';
```


# 3. K3s Deployment

Steps to deploy k3s:
--------------------
- Create new directory k3s-setup inside Documents folder
```
cd Documents
mkdir k3s-setup
```
- Download k3s v3.3.0 from following url:
```
https://gitlab.com/progressoft/devops/reliability/k3s/-/releases
```
- After downloading file move files in k3s-setup folder and extract file
- Extract the tar file:
```
tar -xvzf k3s-v3.3.0.tar.gz -C .
```
Changes made in k3s-run.sh:
----------------------------
export K3S_EXTERNAL_DB=true

export K3S_DB_ENDPOINT="postgres://k3s:k3s@192.168.1.73:5432/postgres?sslmode=disable"

Changes made in k3s-env:
-----------------------
export MASTERS_IPS="192.168.1.73" (Note: Hostmachine IP)

export FLANNEL_NET_OPTS=("enp0s3")

export SSH_USER=mpay

Copied authorized key from readme.md:
-------------------------------------
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCpePg2BzKormiCiDwGTJ8P1mDgsmS6R6dqIktfYKeCdVnIulzSrES022FXmbunmRv0m8dQ2IqmkUYYmEDGfu2GcyMkNalERj0RGFll9VwGbhfb2jeLN4R5WNQpn5FuXC70FMKvSMDCwMj2yF29yRXY31BM9zUseWUIh1QZsScentmhZ43YKUJTg2LsnHLfFZ9gBXKoYZOg4tnXArQm+gDZi0y72E+Yx2zyz3qp07GrVTOxjt8urqwnQ8FhS+vBjpQgnZ1S2Tnq7Eklt1qPesPe5QV8qrF5W+ulotJDMQB3IPqCd0BedOoFcR5J6kEQTiyxahRYBEcXa5LRrtmtiytOBJVrIOHUsDxYKOrOJLnay1E1heBdO8gVRkxZCl0HSIm7nEZ7EWY8GjhKDoP7hdCGNbtMXdxwPxVJF+51n56J/O/eSLwuKljiRhYFXkONufC6LBHaSn2zUs74W3HQfYl+ES/rtUYm54LDZjk83hHd4Dla90hNesigaq0zJOtlJ8s= k3s@installer' >> $HOME/.ssh/authorized_keys

And paste it to terminal:

Connect mpay with k3s by follwoing:
-----------------------------------
```
chmod 600 vagrant.pem
```
- Install openssh-server before doing ssh by follwoing
```
sudo apt-get install openssh-server
```
```
ssh mpay@192.168.1.73 -i vagrant.pem
```
-Exit

Deploy k3s by running following script in k3s home directory:
------------------------------------------------------------

```
./k3s-run.sh
```
- For k3s deployment issues please refer below link:

<a href="https://gitlab.com/progressoft/support/teamnepalrepo/pod-c/induction/-/issues/136"><strong>Deployment Challenges»</strong></a>

- Copy and paste in terminal
```
export KUBECONFIG=/home/mpay/Documents/k3s-setup/k3s-kubeconfig
```
- To check nodes and pods status 
```
sudo kubectl get nodes
sudo kubectl get pods -A

```
- Now to check kine table in postgres connect k3s user in postgresdb by follwoing

To connect k3s user in postgres
--------------------------------
```
psql -U k3s -h localhost -p 5432 -d postgres
```
To list tables in postgres
--------------------------
```
\d
```
<img src="Screenshots/List-of-talbes.png">


- Create new directory mpay-release inside /home/mpay/Documents/
- To download initial dump for k3s deployment:
- Go to https://gitlab.com/progressoft/mobile/mars/yemen/meps-yemen-webapp/-/releases url
- Copy and paste below command to download meps-yemen-webapp from terminal by following: or

```
wget 'https://devops-gitlab.storage.googleapis.com/airgap/progressoft/mobile/mars/yemen/meps-yemen-webapp/meps-yemen-webapp-v1.3.3.tar.gz?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=GOOG1EZ3FR54TE6ZCEFGOKQY2IWNILP4SOXART4QMGQJBXZDUCSPUQZFKAKKY%2F20240206%2FUS-CENTRAL1%2Fs3%2Faws4_request&X-Amz-Date=20240206T214827Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=4ad7809fe44b1e8f2fe73d83aac41646b7a74407b5553b156d9a9e889e27efa2' -O v1.3.3.tar.gz
```
- Extract v1.3.3.tar.gz using following command in mpay-release directory
```
tar -xvzf v1.3.3.tar.gz -C /path/to/directory/
```
- To import images use following command
```
sudo mv images-meps-yemen-webapp-v1.3.3.tar /var/lib/rancher/k3s/agent/images/
```
- Switch terminal to root user to import images

```
ctr images import /var/lib/rancher/k3s/agent/images/images-meps-yemen-webapp-v1.3.3.tar
```
- Exit from root user
- Rename the file stack-0.5.0.tgz to mepsyemen-helm.tgz by following
```
mv stack-0.5.0.tgz mepsyemen-helm.tgz
```
```
sudo mv mepsyemen-helm.tgz /var/lib/rancher/k3s/server/static/charts/
```

- Download database dump file from repo.

- Extract dump
```
tar -xvzf mepsyemendb.tar.gz 
```
- Import database by follwoing:
```
psql -f mepsyemen.sql -h 192.168.1.73 -p 5432 -U mepsyemen -d postgres -v ON_ERROR_STOP=1
```

- Download mepsyemen-training.yaml from repo and copy file in following path

```
sudo mv mepsyemen-training.yaml /var/lib/rancher/k3s/server/manifests/
```
- Configure IP(hostmachine IP) in mepsyemen-taining.yaml file
- mepsyemen-taining.yaml file start deployment of pods automatically.
- To check pods status
```
sudo kubectl get pods -n mpay
```
- We can also check the pods  deployment logs

- Once Deployment success

- Check the services by follwoing

```
sudo kubectl get svc -n mpay
```
- Copy core-ip with port 7070 and paste in browser
 
<img src="Screenshots/ps-mpay-page.png">

-Note: password: sys

- After deployment need following changes in Database:
------------------------------------------------------
- Dowload and install dbeaver and connect to postgres DB and do the follwoing changes.

<img src="Screenshots/DBeaver connection.png">

```
select * from mepsyemen.mpay_sysconfigs where configkey='SPRING URLS' or configkey='MPAY CORE SPRINIG URL' or configkey='JFW URLS' or configkey like 'Enable Send%'

update mepsyemen.mpay_sysconfigs set configvalue='http://mepsyemen-mpay:8080/clearCache' where configkey='SPRING URLS';
update mepsyemen.mpay_sysconfigs set configvalue='http://mepsyemen-mpay:8080/processMessageInternal' where configkey='MPAY CORE SPRINIG URL';
update mepsyemen.mpay_sysconfigs set configvalue='http://mepsyemen-core:7070/clearCache' where configkey='JFW URLS';
update mepsyemen.mpay_sysconfigs  set configvalue='0' where configkey='Enable Send Notification';
```